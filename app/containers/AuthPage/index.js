import React from 'react';
import { Helmet } from 'react-helmet';
import { Button } from 'rsuite';

import H1 from 'components/H1';

export default class AuthPage extends React.Component {
  // Since state and props are static,
  // there's no need to re-render this component
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Auth Page</title>
          <meta name="description" content="Login to Apaltlr" />
        </Helmet>
        <H1>Auth Page</H1>
        <Button appearance="primary">BUTTON</Button>
        <Button appearance="primary">button</Button>
        <Button appearance="primary">Button</Button>
      </div>
    );
  }
}
